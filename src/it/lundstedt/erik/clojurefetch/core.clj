(ns it.lundstedt.erik.clojurefetch.core
  (:gen-class))
(:require '[environ.core])
;;(:require [namespace :as alias])




(def ascii-art
  [
   "  _________________________  "
   "<||==#=================#==||>"
   "<||==|                 |==||>"
   "<||==|                 |==||>"
   "<||==|                 |==||>"
   "<||==|                 |==||>"
   "<||==|                 |==||>"
   "<||==|                 |==||>"
   "<||==|                 |==||>"
   "<||==|                 |==||>"
   "<||==|                 |==||>"
   "<||==|                 |==||>"
   "<||==|                 |==||>"
   "<||==|                 |==||>"
   "<||==|                 |==||>"
   "<||==#=================#==||>"
   "   ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯   "
   ]
  )

(def head [[(System/getProperty "user.name")  "@"
            (System/getProperties ".hostname")
            ]
           ["<|===="  "="  "============|>"]
           ]
  )



(def data
  [
   (get head 0)
   (get head 1)
   [" K " " : " " v "]
   [" K " " : " " v "]
   [" K " " : " " v "]
   [" K " " : " " v "]
   [" K " " : " " v "]
   [" K " " : " " v "]
   [" K " " : " " v "]
   [" K " " : " " v "]
   [" K " " : " " v "]
    ]
  )

(defn mpprint [a b i]
  (str (get a i)
          "     "
      (or (get (get b i) 0) "")
      (or (get (get b i) 1) "")
      (or (get (get b i) 2) "")
      ))

(defn combine
  [a b]
  (dotimes [i (count a)]
    (println
     (mpprint a b i)
     )))

(defn -main
  "I don't do a whole lot ... yet."
 [& args]

;;(doseq [line ascii-art]
;;  (println line)
;;  )
  (combine ascii-art data)



;;    (println i)

  ;;(println "Hello, World!")
  args
  )
